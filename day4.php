<?php

$lines = file('day4_input.txt');
$part1 = $part2 = 0;
$copies = [];
for($i=1; $i <= count($lines); $i++) $copies[$i]=0;
foreach($lines as $nb => $line){
    $result = $draw = 0;
    $data = explode(':',$line);
    preg_match('/\d+/',$data[0],$id);
    $id = (int)$id[0];

    $numbers = explode("|",$data[1]);
    
    preg_match_all('/\d+/',$numbers[0],$winning);
    preg_match_all('/\d+/',$numbers[1],$tirage);

    $winning = $winning[0];
    $tirage = $tirage[0];

    foreach($tirage as $number){
        if(in_array($number, $winning)){
            $result++;
            $draw = $draw == 0 ? 1 : ($draw *2);
        }
    }

    if($result > 0){
        for($i = ($id+1); $i <= ($id+$result); $i++){
            $copies[$i] = $copies[$i]+(1+$copies[$id]);
        }
    }


    $part1 += $draw;

}
$part2 = array_sum($copies) + count($lines);
echo "Part 1 TOTAL : ".$part1."\n";
echo "Part 2 TOTAL : ".$part2."\n";