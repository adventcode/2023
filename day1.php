<?php

define('STRNB' ,["one","two","three","four","five","six","seven","eight","nine"]);
define('NBSTR',array_flip(STRNB));

function intValue($input){
    if(in_array($input, STRNB)){        
        $input = NBSTR[$input]+1;
    }
    return $input;
}

/***
 * 
 * PART ONE
 * 
 */

$lines = file('day1_input.txt');
$counter = 0;

foreach($lines as $line){
    preg_match_all('!\d!', $line, $matches);
    
    $calibrations = $matches[0];

    $value = (int)($calibrations[0] . end($calibrations));

    $counter += $value;
}

echo "Part 1 TOTAL : ".$counter."\n";

/***
 * 
 * PART TWO
 * 
 */

$counter = 0;

foreach($lines as $line){
    
    preg_match_all('!(?=('.join('))|(?=(',STRNB).'))|\d!', $line, $matches, PREG_UNMATCHED_AS_NULL);

    $calibrations = [];
    foreach($matches as $match){
        foreach($match as $key => $value){
            if($value !== NULL) $calibrations[$key]=$value;
        }
    }

    $first = intValue($calibrations[0]);
    $last = intValue(end($calibrations));

    $value = (int)($first.$last);

    $counter += $value;

}

echo "Part 2 TOTAL : $counter\n";