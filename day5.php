<?php
/**
 * TODO: Part 2 ...
 */


class Day5{

    protected $lines;
    protected $part1;
    protected $part2;
    protected $previous_map;
    protected $current_map;
    protected $seeds;
    protected $seed_to_soil;
    protected $soil_to_fertilizer;
    protected $fertilizer_to_water;
    protected $water_to_light;
    protected $light_to_temperature;
    protected $temperature_to_humidity;
    protected $humidity_to_location;

    public function __construct($filename)
    {
        $this->lines = file($filename);
        
        $this->initData();
    }

    public function partOne()
    {
        foreach($this->lines as $nb => $line){
            $line = trim($line);
            //La première ligne contient les graines
            if($nb == 0 ){
                preg_match_all('!\d+!', $line, $seeds);
                $this->seeds = $seeds[0];
                continue;
            }
            
            //On saute les lignes vides
            if(!strlen($line)) continue;

            //On récupère le titre du mapping
            if(substr($line,-4)==='map:'){
                $this->switchMap(substr($line,0,-5));
                continue;
            }

            preg_match_all('!\d+!', $line, $match);
            $this->setValues($match[0][1],$match[0][0],$match[0][2]);

        }
        
        $this->switchMap('seeds');
        $this->part1 = min($this->humidity_to_location);

        echo sprintf("TOTAL PART 1: %d \n",$this->part1);
    }

    public function partTwo()
    {
        
        $this->initData();
        foreach($this->lines as $nb => $line){
            $line = trim($line);
            //La première ligne contient les graines
            if($nb == 0 ){
                preg_match_all('!\d+!', $line, $seeds);
                $seeds = $seeds[0];
                for($i=0;$i<count($seeds);$i+=2){
                    $this->seeds[] = [
                        'start' => $seeds[$i],
                        'range' => $seeds[$i+1],                    ];
                }

                continue;
            }
            
            //On saute les lignes vides
            if(!strlen($line)) continue;

            //On récupère le titre du mapping
            if(substr($line,-4)==='map:'){
                $this->previous_map = $this->current_map;
                $this->current_map = str_replace('-','_',substr($line,0,-5));
                continue;
            }

        }
        echo sprintf("TOTAL PART 2: %d \n",$this->part2);
    }


    protected function initData()
    {
        $this->previous_map = '';
        $this->current_map = 'seeds';
        $this->seeds = [];
        $this->seed_to_soil = [];
        $this->soil_to_fertilizer = [];
        $this->fertilizer_to_water = [];
        $this->water_to_light = [];
        $this->light_to_temperature = [];
        $this->temperature_to_humidity = [];
        $this->humidity_to_location = [];
    }

    protected function setValues($key,$value,$range)
    {
        $current = $this->current_map;
        $previous = $this->previous_map;
        foreach($this->$previous as $id){
            if($id >= $key && $id < ($key+$range)){
                $delta = $id - $key;
                $this->$current[$id] = $value + $delta;
            }

        }
    }

    protected function switchMap($newMap){
        if(strlen($this->previous_map)){
            
            $current = $this->current_map;
            $previous = $this->previous_map;

            if(count($this->$previous) > count($this->$current)){
                $delta = array_diff($this->$previous, array_keys($this->$current));

                foreach($delta as $missing){
                    $this->$current[$missing] = $missing;
                }
            }


        }   
        
        $this->previous_map = $this->current_map;
        $this->current_map = str_replace('-','_',$newMap);
    }

}

$day5 = new Day5('day5_input.txt');
$day5->partOne();
$day5->partTwo();
