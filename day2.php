<?php

define("MAX_G",13);
define("MAX_B",14);
define("MAX_R",12);

$lines = file('day2_input.txt');
$part1 = $part2 = 0;

foreach($lines as $line){
    //On initialise le tableau des cubes
    $cubes = ["red" => 0, "green" => 0, "blue" => 0];
    //On commence par séparer la Game des tirages pour avoir son ID
    $data = explode(':',$line);
    preg_match('/\d+/',$data[0],$match);
    $id = (int)$match[0];

    //On sépare les tirages et on alimente le tableau des cubes
    $tirages = explode(";",$data[1]);
    foreach($tirages as $tirage){
        preg_match_all('/(\d+) (green|red|blue)/',$tirage,$results);
        for($i=0; $i < count($results[0]); $i++){
            $cubes[$results[2][$i]] = $cubes[$results[2][$i]] < $results[1][$i] ? $results[1][$i] : $cubes[$results[2][$i]];
        }
    }

    //On ajoute l'ID uniquement si les conditions sont OK pour la partie 1
    if($cubes["red"]<= MAX_R && $cubes["green"] <= MAX_G && $cubes["blue"] <= MAX_B) $part1 +=$id;
    //On calcul le résultats de la partie 2 :
    $part2 += ($cubes["red"] * $cubes["green"] * $cubes["blue"]);


}

echo "Part 1 TOTAL : ".$part1."\n";
echo "Part 2 TOTAL : ".$part2."\n";