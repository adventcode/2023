<?php
/**
 * TODO: Gros gros refactoring parce bon c'est bien moche tout de même ...
 */


class Day3{

    protected $lines;
    protected $part1 = 0;
    protected $part2 = 0;
    protected $digits = [];
    protected $spchr = [];
    protected $stars = [];
    protected $gears = [];

    public function __construct($filename)
    {
        $this->lines = file($filename);
        
        $this->initData();
    }

    public function partOne()
    {
        foreach($this->digits as $nb => $coord){
            foreach($coord as $pos => $value){
                if ($this->checkProximity($nb,$pos,strlen($value))){
                    $this->part1 += (int)$value;
                } 
            }

        }

        echo sprintf("TOTAL PART 1: %d \n",$this->part1);
    }

    public function partTwo()
    {
        foreach($this->digits as $nb => $coord){
            foreach($coord as $pos => $value){
                $this->populateGears($nb,$pos,strlen($value));
            }
        }
        foreach($this->gears as $line){
            foreach($line as $pos => $parts){
                if(count($parts) > 1){
                    $this->part2 += array_product($parts);
                }
            }
        }
        
        echo sprintf("TOTAL PART 2: %d \n",$this->part2);
    }


    protected function initData()
    {
        foreach($this->lines as $nb => $line){
            //On récupère tous les nombres de la lignes avec leur position
            preg_match_all('!\d+!', $line, $matches,PREG_OFFSET_CAPTURE);
            foreach($matches[0] as $match){
                $this->digits[$nb][$match[1]] = $match[0];
            }
            unset($matches);
        
            //On récupère tous les caractères de la lignes avec leur position
            preg_match_all('![^\.\d^\n]!', $line, $matches,PREG_OFFSET_CAPTURE);
            foreach($matches[0] as $match){
                $this->spchr[$nb][$match[1]] = $match[0];
            }
            
            //On récupère toutes les étoiles de la lignes avec leur position
            preg_match_all('!\*!', $line, $matches,PREG_OFFSET_CAPTURE);
            foreach($matches[0] as $match){
                $this->stars[$nb][$match[1]] = $match[0];
            }
        }
    }

    protected function checkProximity($line, $pos, $length): bool
    {
        //On regarde déjà si un caratcère est sur la même ligne
        if(array_key_exists($line, $this->spchr) && array_key_exists($pos-1, $this->spchr[$line])) return true;
        if(array_key_exists($line, $this->spchr) && array_key_exists($pos+$length, $this->spchr[$line])) return true;
        //On regarde ensuite les lignes autour
        for($i=$pos-1; $i<($pos+$length+1); $i++){
            if(array_key_exists($line-1, $this->spchr) && array_key_exists($i, $this->spchr[$line-1])) return true;
            if(array_key_exists($line+1, $this->spchr) && array_key_exists($i, $this->spchr[$line+1])) return true;

        }

        return false;
    }

    protected function populateGears($line, $pos, $length)
    {
        if(array_key_exists($line, $this->stars) && array_key_exists($pos-1, $this->stars[$line])){
            $this->gears[$line][$pos-1][] = $this->digits[$line][$pos];
            return true;
        } 
        if(array_key_exists($line, $this->stars) && array_key_exists($pos+$length, $this->stars[$line])){
            $this->gears[$line][$pos+$length][] = $this->digits[$line][$pos];
            return true;
        } 
        //On regarde ensuite les lignes autour
        for($i=$pos-1; $i<($pos+$length+1); $i++){
            if(array_key_exists($line-1, $this->stars) && array_key_exists($i, $this->stars[$line-1])){
                $this->gears[$line-1][$i][] = $this->digits[$line][$pos];
                return true;
            } 
            if(array_key_exists($line+1, $this->stars) && array_key_exists($i, $this->stars[$line+1])){
                $this->gears[$line+1][$i][] = $this->digits[$line][$pos];
                return true;
            } 
        }
    }

}

$day3 = new Day3('day3_input.txt');
$day3->partOne();
$day3->partTwo();